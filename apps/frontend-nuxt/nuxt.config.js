const sitemapRoutes = require('./src/utils/sitemap-routes')

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: 'Opswerks',
    meta: [ ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: 'favicon.ico'
      }
    ]
  },
  router: {
    base: '/kim/'
  },

  vue: {
    config: {
      productionTip: false,
      devtools: true
    }
  },

  env: {
    stripeEnv: process.env.stripeEnv,
    stripePublicKey: process.env.stripePublicKey,
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/vuetify-datetime-picker',
    { src: "~/plugins/vue-timer", mode: 'client'},
    { src: '@/plugins/vue-shortkey.js', mode: 'client' },
    '~/plugins/vue2-filters'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    // '@nuxtjs/eslint-module'
    '@nuxtjs/vuetify',
    ['@nuxtjs/google-analytics', {
      id: 'UA-154843930-1'
    }],
    ['nuxt-stripe-module', {
      version: 'v3',
      //TODO: change publishableKey based on environment
      publishableKey: 'pk_test_EiveKNyPoW3C9bpmpEJXuawF'
    }],
    '@nuxtjs/dotenv',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/auth',
    ['nuxt-fontawesome', {
      imports: [
        //import whole set
        {
          set: '@fortawesome/free-solid-svg-icons',
          icons: ['fas']
        },
        {
          set: '@fortawesome/free-brands-svg-icons',
          icons: ['fab']
        }
      ]
    }],
    ['vue-currency-filter/nuxt', {
      symbol: '$',
      thousandsSeparator: ',',
      fractionCount: 0,
      fractionSeparator: '.',
      symbolPosition: 'front',
      symbolSpacing: false
    }],
    '@nuxtjs/robots',
    '@nuxtjs/sitemap'
  ],
  auth: {
   plugins: [ '@/plugins/auth.js' ]
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseURL: process.env.BASE_URL || 'http://aa5675c7cd7e711eab0de0aae28472e5-310048246.us-east-2.elb.amazonaws.com/api/auth'
  },

  robots: {
    Sitemap: ''
  },

  sitemap: {
    exclude: [
      '/account/**',
      '/account',
      '/admin/**',
      '/admin',
    ],
    routes: async () => {
      let baseUrl =  process.env.BASE_URL || 'http://aa5675c7cd7e711eab0de0aae28472e5-310048246.us-east-2.elb.amazonaws.com/api/auth'
      return await sitemapRoutes(baseUrl)
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    publicPath: '/kim/',
    extend (config, ctx) {
    },

    transpile: ['vuetify-datetime-picker']
  },

  server: {
    host: '0.0.0.0',
    port: process.env.PORT || 5000
  },

  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: '/token/login', method: 'post', propertyName: 'token' },
          logout: { url: '/token/logout', method: 'post' },
          user: { url: '/users', method: 'get', propertyName: 'email' }
        },
        // tokenRequired: true,
        tokenType: '',
      },
    },
    redirect: {
      home: false,
      callback: false,
      logout: false
    }
  },
}
