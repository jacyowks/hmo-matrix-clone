import os
import subprocess
import shutil

import argparse


def main(
    TEMPLATE_DIR_PATH: str = "templates",
    RENDERED_DIR_PATH: str = "manifests",
    ALLOWED_FILES: list = ["yaml", "yml"],
    WHITELIST: list = [],
):
    template_dirs = os.walk(TEMPLATE_DIR_PATH)

    # Delete RENDERED_DIR_PATH if it exists so we have a clean directory
    if os.path.exists(RENDERED_DIR_PATH):
        shutil.rmtree(RENDERED_DIR_PATH)

    for template_dir in template_dirs:
        dirname = template_dir[0]
        files = template_dir[2]

        if dirname == TEMPLATE_DIR_PATH:
            rendered_dir = RENDERED_DIR_PATH
        else:
            rendered_dir = "{}/{}".format(
                RENDERED_DIR_PATH, dirname[len(TEMPLATE_DIR_PATH) + 1 : :]
            )

        for filename in files:
            extension = filename.split(".")[-1]

            to_render = True
            if len(WHITELIST) > 0:
                if not filename in WHITELIST:
                    to_render = False
            else:
                if extension not in ALLOWED_FILES:
                    to_render = False

            rendered_filename = "{}/{}".format(rendered_dir, filename)

            if not to_render:
                if not os.path.exists(rendered_dir):
                    os.makedirs(rendered_dir)
                shutil.copyfile("{}/{}".format(dirname, filename), rendered_filename)
            else:
                rendered_output = subprocess.check_output(
                    "envsubst < {}/{}".format(dirname, filename), shell=True
                )

                if not os.path.exists(rendered_dir):
                    os.makedirs(rendered_dir)
                with open(rendered_filename, "w+") as rendered_file:
                    rendered_file.write(rendered_output.decode("utf-8"))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-i", "--input", const="templates", nargs="?", help="Folder to render"
    )
    parser.add_argument(
        "-o",
        "--output",
        const="manifests",
        nargs="?",
        help="Directory to place rendered files. Will create if it does not exist.",
    )
    parser.add_argument(
        "-w",
        "--whitelist",
        nargs="+",
        # const=[],
        help="If passed, program will only render these files (but still copy the whole directory to output)",
    )
    parser.add_argument(
        "-e",
        "--extensions",
        nargs="+",
        # const=["yaml", "yml"],
        help="Only filenames with these extensions will be rendered. Defaults to yaml and yml.",
    )

    args = parser.parse_args()

    default_values = {
        "extensions": ["yaml", "yml"],
        "whitelist": [],
    }

    args = {
        key: value if value is not None else default_values[key] for key, value in vars(args).items()
    }

    main(
        TEMPLATE_DIR_PATH=args["input"],
        RENDERED_DIR_PATH=args["output"],
        ALLOWED_FILES=args["extensions"],
        WHITELIST=args["whitelist"],
    )
